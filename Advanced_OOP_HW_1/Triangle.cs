﻿using System;
namespace Advanced_OOP_HW_1
{
    class Triangle : ISquareCalculatable
    {
        public double Height { get; set; }
        public double Base { get; set; }
        /// <summary>
        /// Function which calculates area of the triangle
        /// </summary>
        /// <param name="_height"></param>
        /// <param name="_base"></param>
        public double Area()
        {

            return 0.5 * Height * Base;
        }
        public Triangle(double _height, double _base)
        {
            Height = _height;
            Base = _base;
        }
        public override string ToString()
        {
            return string.Format("height: {0}  area: {1}", Height, this.Area());
        }
    }
}
