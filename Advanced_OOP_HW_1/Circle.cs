﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_OOP_HW_1
{
    class Circle : ISquareCalculatable
    {
        public double Radius { get; set; }
        public Circle(double _radius)
        {
            Radius = _radius;
        }
        public override string ToString()
        {
            return string.Format("radius: {0} area: {1}", Radius, this.Area());
        }
        /// <summary>
        /// Function which calculates area of the Circle
        /// </summary>
        /// <returns></returns>
        public double Area()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}
