﻿using System;

namespace Advanced_OOP_HW_1
{

    interface ISquareCalculatable
    {
        double Area();
    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle(12, 5.2);
            Triangle t = new Triangle(12, 52);
            Circle c = new Circle(24.25);
            Console.WriteLine(c.ToString());
            Console.WriteLine(t.ToString());
            Console.WriteLine(r.ToString());

            Console.ReadKey();
        }
    }
}
