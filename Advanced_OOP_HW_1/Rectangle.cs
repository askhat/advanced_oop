﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_OOP_HW_1
{
    class Rectangle : ISquareCalculatable
    {
        public double Height { get; set; }
        public double Width { get; set; }


        public override string ToString()
        {
            return string.Format("height: {0} width: {1} area:{2}", Height, Width, this.Area());
        }
        public Rectangle(int _height, double _width)
        {
            Height = _height;
            Width = _width;
        }
        /// <summary>
        /// Function which calculates area of the Rectangle
        /// </summary>
        /// <returns></returns>
        public double Area()
        {
            return Height * Width;
        }

    }
}
